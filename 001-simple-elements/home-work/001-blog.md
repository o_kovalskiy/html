# Blog feed page
## Create a page according to mockup 


### Requirements:
* Content container width is `630px`;
* Colors:
    + Page background: #333F44;
    + `Header` title: #FFFFFF (white);
    + `Nav` text: #FFFFFF;
    + Post title: #9DEDDB;
    + Post text: #CFCFCF;
    + Read More link: #DEE5AB;
    + Button bakcground: #CFCFCF;
    + Button text: #333F44;
    + `Footer` logo: white;
* Font Sizes:
    + `Header` title: 32px;
    + `Nav` text: 18px;
    + Post title: 24px;
    + Post text: 16px;
    + Read more text: 16px;
    + Button text: 12px;
    + `Footer` logo: 32px;
* Font Families:
    + All text besides `nav` is [Roboto Slab](https://fonts.google.com/specimen/Roboto+Slab)
    + `Nav` text is [Quicksand](https://fonts.google.com/specimen/Quicksand)
* Button should be clickable and lead to another page (placeholder link)
* Nav items should be clickable as well and open in the same tab (placeholder link)
* Read more - clickable
---

    