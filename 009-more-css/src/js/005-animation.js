var download = document.querySelector(".download");
var loading = document.querySelector(".loading");
console.log(loading)
function offAnimation() {
  if (loading.style.animationPlayState != "paused") {
    loading.style.animationPlayState = "paused";
  } else {
    loading.style.animationPlayState = "running";
  }
}

download.addEventListener("click", offAnimation);
