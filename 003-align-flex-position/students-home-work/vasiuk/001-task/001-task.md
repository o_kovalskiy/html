# Sliding box

Open in browser index.html which is in `src` folder

<img src="media/green-box.png" width="300"/>

If you click on the `Item one` the grey box will slide to the right.<br/>
Try.<br/>
If you click again, the box will hide. It just goes round and round.

### The goal - need to understand how `javascript` works, and try to write the same stuff (use the same javascript code and prepare an explanation about how do you think it works to the next lesson )

### Requirements:

* Create the same scenario using an existing javascript code
* Prepare an explanations