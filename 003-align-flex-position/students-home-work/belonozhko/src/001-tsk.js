var item = document.getElementById("one");                 // Обращаемся к элементу по id "one"
var sideBar = document.querySelectorAll(".side-bar");      // Обращаеемся к классу ".side-bar"

function toggleSideBar () {                              // Вызываем функцию переключателя
   sideBar.forEach(function(element) {                   // Задаем условия переключения функции для каждого элемента 

        if (element.style.left == "0px") {               // Если у элемента 0px при переключении будет 200
           element.style.left = "200px";                 // Если у элемента 200px при переключнеии возвращаем 0
        }
        else {
            element.style.left = "0px";
        }
   }, this); 
}

item.addEventListener('click', toggleSideBar);         // Добавляем возможнось клика