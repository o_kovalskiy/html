﻿var event = document.getElementById('event')
var backdrop = document.getElementById('backdrop')
var closeBtn = document.getElementById('close')

event.addEventListener('click', showDialog)
closeBtn.addEventListener('click', closeDialog)

function showDialog() {
    backdrop.style.display = "block"
}

function closeDialog() {
    backdrop.style.display = "none"
}