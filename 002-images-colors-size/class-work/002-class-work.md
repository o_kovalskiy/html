# Columns with text

## Create a page with columns which can have different number of a text

### Requirements:
* Colors:
    + columns: rgba(255, 255, 255, 0.34);
    + text: #225684;
* Shadows:
    + column shadow: 0px 0px 38px rgba(0, 0, 0, 0.08);
* Fonts sizes:
    + column title: 26px; bold;
    + column text: 18px;
* Font families:
    + Lato
* Material Icons: 
    + restaurant_menu
    + flight
    + directions_bike