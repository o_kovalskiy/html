# Camp Home

## Create a Home page according to mockup

### Requirements:
* Colors:
    + welcome to camp section: #39C0C3
    + text: black and white
    + coloured text background: rgba(19, 20, 21, 0.7)
    + button: white
* Shadows:
    + coloured text background: 2px solid rgba(0, 0, 0, 0.34)
    + button: 2px 2px 1px rgba(128, 128, 128, 0.47)
* Fonts sizes:
    + nav: 18px
    + section title: 36px;
    + section subtitle: 24px
    + button: 18px;
    + coloured section text: 24px;
* Font families:
    + Open Sans
* Material Icons: 
    + restaurant_menu
    + flight
    + directions_bike